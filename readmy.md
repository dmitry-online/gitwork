# Установка веб-сайта

## Prod
1. composer install
2. создать файл в корневой директории .env (шаблон .env.example)
3. npm install  // for windows add --no-bin-links
4. npm install -g cross-env gulp-cli
5. php artisan key:generate
6. php artisan migrate

##  Dev
composer run-script post-update-cmd

# Сборка статики

## Prod
npm run production

## Dev
npm run development

## Example config for nginx
    server {
        listen 80;
        client_max_body_size 60M;
        root /web/gitwork/public;
        set $bootstrap "index.php";
    
        server_name gitwork.dev;
    
        location ~ ^/(framework|src|vendor) {
            deny all;
            access_log off;
            log_not_found off;
        }
    
        # отключаем обработку запросов фреймворком к несуществующим статичным файлам
        location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|doc|otf|mov|fla|zip|rar)$ {
            access_log off;
            log_not_found off;
        }
    
         location / {
            index index.html $bootstrap;
            try_files $uri $uri/ /$bootstrap?$args;
        }
    
        location ~ \.php$ {
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/run/php/php7.1-fpm.sock;
            fastcgi_index index.php;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param PATH_INFO $fastcgi_path_info;
        }
    }
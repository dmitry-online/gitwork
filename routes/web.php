<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'panel\WorkController@index')->name('panel');


// Frontend
Route::get('/login', 'frontend\FrontendController@login')->name('login');
Route::post('/login', 'frontend\Auth\LoginController@login')->name('login');

Route::get('/register', 'frontend\FrontendController@register')->name('register');
Route::post('/register', 'frontend\Auth\RegisterController@register')->name('register');

Route::get('/reset-password', 'frontend\FrontendController@resetPassword')->name('reset-password');
Route::post('/reset-password', 'frontend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('reset-password');
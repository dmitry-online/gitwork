<?php

namespace App\Http\frontend;

use Illuminate\Http\Request;

class FrontendController extends BaseController
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function login()
    {
        return view('frontend/login');
    }

    public function register()
    {
        return view('frontend/register');
    }

    public function resetPassword()
    {
        return view('frontend/passwordReset');
    }
}

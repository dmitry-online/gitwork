<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <script type="text/javascript" src="{!! asset('panel/js/app.js') !!}"></script>
    <link type="text/css" media="all" rel="stylesheet" href="{!! asset('panel/css/app.css') !!}">
</head>
<body class="gray-bg">
<div id="wrapper">
    <div class="container">
        @yield('content')
    </div>
    <div class="footer">
        <div>
            <strong>Copyright</strong> Git Work &copy; 2016-2019
        </div>
    </div>
</div>
</body>
</html>

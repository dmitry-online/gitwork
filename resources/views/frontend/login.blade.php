@extends('frontend.layouts.form')

@section('title', 'Авторизация в GitWork')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h1 class="logo-name" style="margin-right: 20px;">GW</h1>
        </div>
        <h3>Добро пожаловать в GitWork</h3>
        <p>
            Авторизуйтесь что бы подключиться к нашей сети
        </p>
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" placeholder="Email" required>
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="Пароль" required>
            </div>
            @if ($errors->has('email') || $errors->has('password'))
                <span class="help-block">
                    <strong>Неверный email или пароль</strong>
                </span>
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary block full-width m-b">Войти</button>
            </div>

            <div style="color:#999;margin:1em 0">
                Забыли свой пароль ? <a> Восстановить </a>
            </div>
        </form>
        <a class="btn btn-sm btn-white btn-block" href="{{url('sign-up')}}">Создать новый аккаунт</a>
    </div>
@endsection

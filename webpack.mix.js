let mix = require('laravel-mix');

// MODULE Admin
// mix.js('resources/assets/admin/js/app.js', 'public/admin/js')
//    .sass('resources/assets/admin/sass/app.scss', 'public/admin/css');

// MODULE Frontend
// mix.js('resources/assets/frontend/js/app.js', 'public/frontend/js')
//   .sass('resources/assets/frontend/sass/app.scss', 'public/frontend/css');

// MODULE Panel
mix.js('resources/assets/panel/js/app.js', 'public/panel/js')
  .sass('resources/assets/panel/sass/app.scss', 'public/panel/css');
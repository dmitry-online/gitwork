@extends('frontend.layouts.form')

@section('title', 'Регистрация в GitWork')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h1 class="logo-name" style="margin-right: 20px;">GW</h1>
        </div>
        <h3>Добро пожаловать в GitWork</h3>
        <p>
            Зарегистрируйтесь что бы подключиться к нашей сети
        </p>
        <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" placeholder="Email" required>
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" placeholder="Пароль" required>
            </div>
            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="form-control " name="password_confirmation" placeholder="Подтвердите пароль" required>
            </div>
            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>Пароли не совпадают</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>Пароль слишком лёгкий</strong>
                </span>
            @endif
            @if ($errors->has('email'))
                {{$errors->has('email')}}
                <span class="help-block">
                    <strong>Email уже зарегистрирован</strong>
                </span>
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-primary block full-width m-b">Зарегистрироваться</button>
            </div>

            <div style="color:#999;margin:1em 0">
                Уже зарегистрированы ? <a href="{{url('login')}}"> Войти </a>
            </div>
        </form>
        <a class="btn btn-sm btn-white btn-block" href="{{url('/')}}">Вернуться на главную</a>
    </div>
@endsection

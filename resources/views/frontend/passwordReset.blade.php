@extends('frontend.layouts.form')

@section('title', 'Восстановление пароля в GitWork')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <h1 class="logo-name" style="margin-right: 20px;">GW</h1>
    </div>
    <h3>Забыли пароль?</h3>
    <p>
        Для восстановления доступа к аккаунту заполните форму ниже
    </p>
    <form method="POST" action="{{ route('reset-password') }}">
        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
        </div>
        @if ($errors->has('email'))
        <span class="help-block">
                    <strong>Email не зарегистрирован</strong>
                </span>
        @endif
        <div class="form-group">
            <button type="submit" class="btn btn-primary block full-width m-b">Восстановить доступ</button>
        </div>

        <div style="color:#999;margin:1em 0">
            <a href="{{url('/')}}">Вернуться на  главную </a>
        </div>
    </form>
</div>
@endsection

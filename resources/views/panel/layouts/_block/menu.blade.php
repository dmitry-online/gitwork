<div class="container">
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Git Work</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Request::is('panel') ? 'active' : '' }}">
                    <a href="{{url('panel')}}">Работа</a>
                </li>
                <li class="{{ Request::is('freelancers') ? 'active' : '' }}">
                    <a href="{{url('freelancers')}}">Фрилансеры</a>
                </li>
                <li class="{{ Request::is('reports') ? 'active' : '' }}">
                    <a href="{{url('reports')}}">Отчеты</a>
                </li>
                <li class="{{ Request::is('messages') ? 'active' : '' }}">
                    <a href="{{url('messages')}}">Сообщения</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="{{ Request::is('profile') ? 'active' : '' }}">
                    <a href="{{url('profile')}}">Профиль</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
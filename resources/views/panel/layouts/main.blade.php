<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <script type="text/javascript" src="{!! asset('panel/js/app.js') !!}"></script>
    <link type="text/css" media="all" rel="stylesheet" href="{!! asset('panel/css/app.css') !!}">
</head>
<body class="top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg navbar-shadow">
            @include('panel/layouts/_block/menu')
        </div>

        <div class="wrapper wrapper-content">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="footer">
        <div>
            <strong>Copyright</strong> Git Work &copy; 2016-2019
        </div>
    </div>
</div>
</body>
</html>
